const mongoose = require("mongoose");
const ownerSchema = new mongoose.Schema({
    userId:{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    size: {
        type: String,
        required: true
    },
    province: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    budget: {
        type: String,
        required: true
    },
    time: {
        type: String,
        required: true
    },
    breakdown1: {
        type: String,
        required: true
    },
    breakdown2: {
        type: String,
        required: true
    },
    breakdown3: {
        type: String,
        required: true
    },
    status:{
        type:String,
        default:"uploaded"
    },
    bid:{
        type: Array,
        default: []
    },
    date: {
        type: Date,
        default: Date.now()
    },
})
module.exports = mongoose.model("Owner", ownerSchema);