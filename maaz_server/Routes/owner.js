const express =require("express")
const ownerController=require("../Controller/ownerController")
const ownerRouter=express.Router()

ownerRouter.post("/postData",ownerController.postData);
ownerRouter.get("/getData",ownerController.getData);
ownerRouter.get("/getOwnerListings",ownerController.getOwnerListings);
ownerRouter.get("/delete",ownerController.deleteData);
ownerRouter.post("/updateStatus",ownerController.chaneStatus);
ownerRouter.post("/addBid",ownerController.addBid);





module.exports = ownerRouter