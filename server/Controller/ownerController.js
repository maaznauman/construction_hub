const Owner = require("../Model/Owner.js");
const postData = async (req, res) => {
    const { title, description, type, size, province, city, budget, time, breakdown1,breakdown2,breakdown3, userId} = req.body;
    console.log(breakdown2,breakdown3, userId);
    try {
        const newProject = Owner({userId:userId, title, description, type, size, province, city, budget, time, breakdown1,breakdown2,breakdown3 });
        const saveOwner = await newProject.save();

        if (saveOwner) {
            res.status(200).json("Project Saved Sucessfully")
        }
    } catch (error) {
        return res.status(500).json("Server Side Error");
    }
}
const getData = async (req, res) => {
    try {
        const findAllData = await Owner.find();
        if (!findAllData) {
            return res.status(404).json("no data");
        }
        res.status(200).json(findAllData);
    } catch (error) {
        return res.status(500).json("Server Side Error");
    }
}
const chaneStatus = async (req, res) => {
    const {id, status} = req.body
    try {
        const updateData = await Owner.updateOne({id:id}, {$set: {status : status}})
        if (!updateData) {
            return res.status(401).json("Data Not updated");
        }
        res.status(200).json("Data Updated");
    } catch (error) {
        return res.status(500).json("Server Side Error");
    }
}
const getOwnerListings = async (req, res) => {
    const {id}= req.headers;
    
    try {
        console.log(req.headers.id);
        const findUserData = await Owner.find({userId : id});
        if (!findUserData) {
            return res.status(404).json("no data")
        }
        res.status(200).json(findUserData);
    } catch (error) {
        return res.status(500).json("Server Side Error");
    }
}
const deleteData = async (req, res) => {
    const {id}= req.headers;
    try {
        console.log(req.headers.id);
        const findUserData = await Owner.deleteOne({_id : id});
        if (findUserData) {
            res.status(200).json("Deleted");
        }
    } catch (error) {
        return res.status(500).json("Server Side Error");
    }
}
const addBid = async (req, res) => {
    const {userId, price}= req.body;
    const {id}= req.headers;
    try {
        const bid = {
            "userId":userId,
            "price":price
        }
        const findUserData = await Owner.updateOne({_id : id}, {$push:{bid: bid}});
        if (findUserData) {
            res.status(200).json("Pushed");
        }
    } catch (error) {
        return res.status(500).json("Server Side Error");
    }
}
module.exports = {
    postData,
    getData,
    getOwnerListings,
    deleteData,
    chaneStatus,
    addBid
}