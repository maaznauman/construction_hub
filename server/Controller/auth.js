const User = require("../Model/User.js");
const postData = async (req, res) => {
    const { firstName, lastName, email, password, contactInfo, gender } = req.body;
    console.log(firstName, lastName, email, password, contactInfo, gender);
    try {
        const checkUser = await User.findOne({ email: email });
        // console.log();
        if (checkUser) {
            return res.status(402).json("User Already Exsists")
        }
        const newUser = User({ firstName, lastName, email, password, contactInfo, gender });
        if (!newUser) {
            return res.status(400).json("Unknown error occured while creating user");
        }
        const saveUser = await newUser.save();
        if (saveUser) {
            res.status(200).json("User Saved Sucessfully")
        }
    } catch (error) {
        return res.status(500).json("Server Side Error");
    }
}
const signIn = async (req, res) => {
    const { email, password } = req.body;
    console.log(email,password)
    try {
        const checkUser = await User.findOne({ email: email });
        if (!checkUser) {
            return res.status(404).json("User Does not exsists")
        }
        if(password !== checkUser.password){
            return res.status(401).json("Password Incorrect")
        }
        res.status(200).json(checkUser);
    } catch (error) {
        return res.status(500).json("Server Side Error");
    }
}
module.exports = {
    postData,
    signIn
}