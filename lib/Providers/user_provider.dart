import 'package:flutter/cupertino.dart';
import 'package:fyp_construction_hub/Model/user_model.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

class UserProvider extends ChangeNotifier {
  UserModel _user = UserModel(
    '',
    '',
    '',
    '',
    '',
    '',
    '',
  );
  UserModel get user => _user;
  void setUser(String user) {
    _user = UserModel.fromJson(user);
    notifyListeners();
  }
}