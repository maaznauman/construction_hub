import 'package:flutter/foundation.dart';
import 'package:fyp_construction_hub/Model/project_model.dart';
import 'package:flutter/material.dart';

class ProjectProvider extends ChangeNotifier {
  List<ProjectModel> _listings = [];
  List<ProjectModel> get listings => _listings;

  void setListings(List<ProjectModel> listings) {
    _listings = listings;
    notifyListeners();
  }

  void deleteUserData(String token) {
    _listings.clear();
    notifyListeners();
  }
}