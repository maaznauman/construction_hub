import 'package:flutter/material.dart';
import 'package:fyp_construction_hub/Model/bid_model.dart';
import 'package:fyp_construction_hub/Model/project_model.dart';
import 'package:fyp_construction_hub/Model/user_model.dart';
import 'package:fyp_construction_hub/Providers/owner_project_provider.dart';
import 'package:fyp_construction_hub/Providers/user_provider.dart';
import 'package:fyp_construction_hub/commons/customText.dart';
import 'package:fyp_construction_hub/commons/error_handling.dart';
import 'package:fyp_construction_hub/pages/Contractor/Available%20Projects/available_projects.dart';
import 'package:fyp_construction_hub/pages/Contractor/contractor.dart';
import 'package:fyp_construction_hub/pages/Home/home.dart';
import 'package:fyp_construction_hub/pages/Project%20Owner/project_owner.dart';
import 'package:fyp_construction_hub/pages/Project%20Owner/manageProjects.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class AuthServices {
  void SignUpUser(
      {required BuildContext context,
      required String firstName,
      required String lastName,
      required String email,
      required String password,
      required String contactInfo,
      required String gender}) async {

    UserModel user = UserModel(
        '', firstName, lastName, email, password, contactInfo, gender);
    http.Response res = await http.post(
        Uri.parse('http://137.184.247.201:3001/auth/postData'),
        headers: {"Content-Type": "application/json"},
        body: user.toJson());
    print(res.statusCode);
    if (res.statusCode == 200) {
      Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
    }
  }

  void signIn({
    required BuildContext context,
    required email,
    required password,
  }) async {
    UserModel user = UserModel('', '', '', email, password, '', '');
    http.Response res = await http.post(
      Uri.parse('http://137.184.247.201:3001/auth/signIn'),
      body: user.toJson(),
      headers: {"Content-Type": "application/json"},
    );
    print(res.statusCode);
    if (res.statusCode == 200) {
      SharedPreferences prefs =
          await SharedPreferences.getInstance(); //getting instance
      Provider.of<UserProvider>(context, listen: false)
          .setUser(res.body); //saving data in app memory
      // await prefs.setString(
      //     "X-auth-token", jsonDecode(res.body)['token']);
      Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
    }
  }

  void PostProject({
    required BuildContext context,
    required String userId,
    required String title,
    required String description,
    required String type,
    required String size,
    required String province,
    required String city,
    required String budget,
    required String time,
    required String breakdown1,
    required String breakdown2,
    required String breakdown3,
  }) async {
    ProjectModel project = ProjectModel("",userId, title, description, type, size,
        province, city, budget, time, breakdown1, breakdown2, breakdown3, '',[]);
    http.Response res = await http.post(
        Uri.parse('http://137.184.247.201:3001/owner/postData'),
        headers: {"Content-Type": "application/json"},
        body: project.toJson());
    httpErrorHandle(
        response: res,
        context: context,
        onSuccess: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => ProjectOwner()));
        });
    if (res.statusCode == 200) {
      const SnackBar(
          content: CustomText(
        color: Colors.white,
        fontWeight: FontWeight.w600,
        fontSize: 22.0,
        text: 'Listing Saved',
      ));
    }
  }
  void GetProjects({
    required BuildContext context,
  }) async {
    print("fetchedListings");
    final user = Provider.of<UserProvider>(context, listen: false).user;
    http.Response res = await http.get(
        Uri.parse('http://137.184.247.201:3001/owner/getOwnerListings'),
        headers: {
          "Content-Type": "application/json",
          "id": user.id
        },);
    int count = 0;
    httpErrorHandle(
        response: res,
        context: context,
        onSuccess: () async {
          List<dynamic> jsonData = json.decode(res.body);
          final List<ProjectModel> fetchedListings = [];
          final List<BidModel> fetchedBid = [];
          for (var item in jsonData) {
            if (item is Map<String, dynamic>) {
              final String id = item['_id'];
              final String userId = item['userId'];
              final String title = item['title'];
              final String description = item['description'];
              final String type = item['type'];
              final String size = item['size'];
              final String province = item['province'];
              final String city = item['city'];
              final String budget = item['budget'];
              final String time = item['time'];
              final String breakdown1 = item['breakdown1'];
              final String breakdown2 = item['breakdown2'];
              final String breakdown3 = item['breakdown3'];
              final String status = item['status'];
              List<dynamic> bidData = item['bid'];
              if(bidData.length>1){
                for (var item in bidData) {
                  print(count);
                  count++;
                  if (item is Map<String, dynamic>) {
                    final String userId = item['userId'];
                    final String price = item['price'];
                    BidModel bid = BidModel("", "price");
                    bid = BidModel(userId, price);
                    fetchedBid.add(bid);
                  }
                }
              }
              ProjectModel listing = ProjectModel("", "", "", "", "", "", "", "", "", "", "", "","","",[]);
              listing = ProjectModel(id, userId, title, description, type,
                  size, province, city, budget, time, breakdown1, breakdown2, breakdown3,status,fetchedBid);
              fetchedListings.add(listing);
            }
          }
          Provider.of<ProjectProvider>(context, listen: false).setListings(fetchedListings);
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => const ManageProjects()));
        });
    if (res.statusCode == 200) {
      const SnackBar(
          content: CustomText(
            color: Colors.white,
            fontWeight: FontWeight.w600,
            fontSize: 22.0,
            text: 'Listing Saved',
          ));
    }
  }
  void GetAllProjects({
    required BuildContext context,
  }) async {
      http.Response res = await http.get(
      Uri.parse('http://137.184.247.201:3001/owner/getData'),
      headers: {
        "Content-Type": "application/json",
      },);
    int count = 0;
    httpErrorHandle(
        response: res,
        context: context,
        onSuccess: () async {
          List<dynamic> jsonData = json.decode(res.body);
          final List<ProjectModel> fetchedListings = [];
          final List<BidModel> fetchedBid = [];
          for (var item in jsonData) {
            if (item is Map<String, dynamic>) {
              final String id = item['_id'];
              final String userId = item['userId'];
              final String title = item['title'];
              final String description = item['description'];
              final String type = item['type'];
              final String size = item['size'];
              final String province = item['province'];
              final String city = item['city'];
              final String budget = item['budget'];
              final String time = item['time'];
              final String breakdown1 = item['breakdown1'];
              final String breakdown2 = item['breakdown2'];
              final String breakdown3 = item['breakdown3'];
              final String status = item['status'];
              ProjectModel listing = ProjectModel("", "", "", "", "", "", "", "", "", "", "", "","","",[]);
              listing = ProjectModel(id, userId, title, description, type,
                  size, province, city, budget, time, breakdown1, breakdown2, breakdown3,status,fetchedBid);
              fetchedListings.add(listing);
            }
          }
          Provider.of<ProjectProvider>(context, listen: false).setListings(fetchedListings);
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => const Contractor()));
        });
    if (res.statusCode == 200) {
      const SnackBar(
          content: CustomText(
            color: Colors.white,
            fontWeight: FontWeight.w600,
            fontSize: 22.0,
            text: 'Listing Saved',
          ));
    }
  }
  void updateStatus({
    required BuildContext context,
    required status,
    required id,
  }) async {
    ProjectModel user = ProjectModel(id,"","","","","","","","","","","","",status,[]);
    http.Response res = await http.post(
      Uri.parse('http://137.184.247.201:3001/auth/signIn'),
      body: user.toJson(),
      headers: {"Content-Type": "application/json"},
    );
    if (res.statusCode == 200) {
      print(res.statusCode);
    }
  }
  void addBid({
    required BuildContext context,
    required id,
    required price
  }) async {
    final user = Provider.of<UserProvider>(context, listen: false).user;
    BidModel bid = BidModel(user.id,price);
    http.Response res = await http.post(
      Uri.parse('http://137.184.247.201:3001/owner/addBid'),
      body: bid.toJson(),
      headers: {
        "Content-Type": "application/json",
        "id": id
      },
    );
    Navigator.push(context, MaterialPageRoute(builder: (context)=>AvailableProjects()));
    if (res.statusCode == 200) {
      print(res.statusCode);
    }
  }
  void DeleteProject({
    required BuildContext context,
    required String id
  }) async {
    http.Response res = await http.get(
      Uri.parse('http://137.184.247.201:3001/owner/delete'),
      headers: {
        "Content-Type": "application/json",
        "id": id
      },);
    print(res.statusCode);
    httpErrorHandle(
        response: res,
        context: context,
        onSuccess: () {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>ProjectOwner()));
        });
  }
}
