import 'package:flutter/material.dart';
import 'package:fyp_construction_hub/Universal%20Variabkes/universal_variables.dart';
import 'package:fyp_construction_hub/pages/Project%20Owner/Upload%20a%20project/budget.dart';
import 'package:fyp_construction_hub/pages/Project%20Owner/Upload%20a%20project/detail_into.dart';
import 'package:fyp_construction_hub/pages/Project%20Owner/Upload%20a%20project/sizenLocation.dart';
import 'package:fyp_construction_hub/pages/Project%20Owner/Upload%20a%20project/timeline.dart';
class InitialPages extends StatefulWidget {
  const InitialPages({super.key});

  @override
  State<InitialPages> createState() => _InitialPagesState();
}

class _InitialPagesState extends State<InitialPages> {
  UniversalVaraibles varaibles = UniversalVaraibles();
  int _page = 0;
  List<Widget> pages= [
    DetailsInfoo(),
    SizeLocation(title: "",des: "",type: "",),
    Budget(title: "",des:"", type:"", size:"", province:"", city:""),
    const TimeLines()
  ];
  _InitialPagesState();
  void update_page(int page){
    setState(() {
      _page = page;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pages[_page],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _page,
        onTap: update_page,
        items: [
          const BottomNavigationBarItem(
            label: "FIrst",
              icon: Icon(
                Icons.person_outline,
                color: Colors.black,
              )
          ),
          const BottomNavigationBarItem(
              label: "Sec",
              icon: Icon(
                Icons.view_timeline_outlined,
                color: Colors.black,
              )
          ),
          BottomNavigationBarItem(
              label: "Third",
              icon: Container(
                  child: const Icon(
                    Icons.timeline,
                    color: Colors.black,
                  )
              )
          ),

        ],
      ),
    );
  }
}