import 'package:flutter/material.dart';
import 'package:fyp_construction_hub/Auth%20Services/auth_services.dart';
import 'package:fyp_construction_hub/Providers/user_provider.dart';
import 'package:provider/provider.dart';
class Budget extends StatelessWidget {
  String title, des, type, size, province, city;
  Budget(
      {super.key,
      required this.title,
      required this.des,
      required this.type,
      required this.size,
      required this.province,
      required this.city});
  AuthServices services = AuthServices();
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProvider>(context).user;
    TextEditingController _totalBudgetController = TextEditingController();
    TextEditingController _timeReqController = TextEditingController();
    TextEditingController _breakDown1Controller = TextEditingController();
    TextEditingController _breakDown2Controller = TextEditingController();
    TextEditingController _breakDown3Controller = TextEditingController();
    void postData() {
      print("user.id ${user.id}");
      services.PostProject(
          context: context,
          userId:user.id,
          title: title,
          description: des,
          type: type,
          size: size,
          province: province,
          city: city,
          budget: _totalBudgetController.text,
          time: _timeReqController.text,
          breakdown1: _breakDown1Controller.text,
          breakdown2: _breakDown1Controller.text,
          breakdown3: _breakDown1Controller.text);
    }

    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'Budget details',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          backgroundColor: const Color.fromRGBO(93, 93, 93, 1.0),
        ),
        body: Container(
          decoration: const BoxDecoration(
            color: Colors.grey,
          ),
          child: Padding(
            padding: const EdgeInsets.only(
                top: 20.0, bottom: 20.0, right: 10.0, left: 10.0),
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: Colors.grey,
                        image: const DecorationImage(
                          opacity: 0.5,
                          image:
                              AssetImage("images/Constructionhub_Logo_02.png"),
                        )),
                  ),
                ),
                Opacity(
                  opacity: 0.5,
                  child: Container(
                    color: Colors
                        .white, // Optional: Add a color overlay for better visibility
                  ),
                ),
                ListView(
                  children: [
                    const Center(
                      child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          "Budget Details and Payment schedule",
                          style: TextStyle(
                              fontSize: 20.0, fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        "Approximate Total budget",
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.w600),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: TextField(
                        controller: _totalBudgetController,
                        decoration: const InputDecoration(
                            label: Text("Enter Amount Here",
                                style: TextStyle(fontWeight: FontWeight.w600))),
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        "Approximate Time required",
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.w600),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: TextField(
                        controller: _timeReqController,
                        decoration: const InputDecoration(
                            label: Text(
                          "Enter the time required Here",
                          style: TextStyle(fontWeight: FontWeight.w600),
                        )),
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        "Provide a break down for the payment schedule",
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.w600),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: TextField(
                        controller: _breakDown1Controller,
                        decoration: const InputDecoration(
                            label: Text(
                          "Enter breakdown Here",
                          style: TextStyle(fontWeight: FontWeight.w600),
                        )),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: TextField(
                        controller: _breakDown2Controller,
                        decoration: const InputDecoration(
                            label: Text(
                          "Enter breakdown Here",
                          style: TextStyle(fontWeight: FontWeight.w600),
                        )),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: TextField(
                        controller: _breakDown3Controller,
                        decoration: const InputDecoration(
                            label: Text(
                          "Enter breakdown Here",
                          style: TextStyle(fontWeight: FontWeight.w600),
                        )),
                      ),
                    ),
                    ElevatedButton(
                        onPressed: () => {
                          postData()
                            },
                        child: const Text("Submit")),
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
