import 'package:flutter/material.dart';
import 'package:fyp_construction_hub/Universal%20Variabkes/universal_variables.dart';
import 'package:fyp_construction_hub/pages/Home/home.dart';
import 'package:fyp_construction_hub/pages/Project%20Owner/Upload%20a%20project/sizenLocation.dart';
class DetailsInfoo extends StatefulWidget {
  const DetailsInfoo({super.key});

  @override
  State<DetailsInfoo> createState() => _DetailsInfooState();
}

class _DetailsInfooState extends State<DetailsInfoo> {
  TextEditingController _titleController = TextEditingController();
  TextEditingController _desontroller = TextEditingController();
  String? _typeController;
  final List<String> dropDown = ["Residential","Commercial","Industrial"];
  UniversalVaraibles varaibles = UniversalVaraibles();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Project Owner Dashboard',
            style: TextStyle(
              color: Colors.white,
            ),),
          backgroundColor: const Color.fromRGBO(93, 93, 93, 1.0),
        ),

        body: Padding(
          padding: const EdgeInsets.only(top: 0.0, bottom: 20.0, right: 10.0, left: 10.0),
          child: Stack(children: [
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20.0),
                  color: Colors.grey,
                  image: const DecorationImage(
                    opacity: 0.5,
                    image: AssetImage("images/Constructionhub_Logo_02.png"),
                  )
              ),
            ),
            Opacity(opacity: 0.5,child: Container(
              color: Colors.white, // Optional: Add a color overlay for better visibility
            ),),
            ListView(children: [
              Center(
                child: Text("Details and Info",
                  style: TextStyle(
                      fontSize: 28.0,
                      fontWeight: FontWeight.w600
                  ),),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text("Project Title",
                  style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.w600
                  ),),
              ),
              Padding(
                padding: EdgeInsets.only(left: 8.0, right: 8.0),
                child: TextField(
                  controller: _titleController,
                  decoration: InputDecoration(
                      label: Text("Enter Title Here")
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text("Project Description",
                  style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.w600
                  ),),
              ),
              Padding(
                padding: EdgeInsets.only(left: 8.0, right: 8.0),
                child: TextField(
                  controller: _desontroller,
                  decoration: InputDecoration(
                      label: Text("Enter Title Here")
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text("Project Type",
                  style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.w600
                  ),),
              ),
              DropdownButtonFormField<String>(
                  value: dropDown[0],
                  items: dropDown.map<DropdownMenuItem<String>>((String value)
                  {
                    return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value)
                    );
                  }).toList(),
                  onChanged: (String? value){
                    _typeController = value;
                  }
              ),
              ElevatedButton(
                  onPressed: ()=>{
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>SizeLocation(title: _titleController.text, des: _desontroller.text, type: _typeController.toString(),)))
                  },
                  child: Text("Next")
                  ),
            ],)
          ],),
        )
    );
  }
}

