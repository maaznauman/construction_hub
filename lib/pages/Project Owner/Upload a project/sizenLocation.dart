import 'package:flutter/material.dart';
import 'package:fyp_construction_hub/pages/Project%20Owner/Upload%20a%20project/budget.dart';

class SizeLocation extends StatelessWidget {
  String title, des, type;
  SizeLocation(
      {super.key, required this.title, required this.des, required this.type});
  TextEditingController size = TextEditingController();
  TextEditingController province = TextEditingController();
  TextEditingController city = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'Project Owner Dashboard',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          backgroundColor: const Color.fromRGBO(93, 93, 93, 1.0),
        ),
        body: Padding(
          padding: const EdgeInsets.only(
              top: 0.0, bottom: 20.0, right: 10.0, left: 10.0),
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: Colors.grey,
                    image: const DecorationImage(
                      opacity: 0.5,
                      image: AssetImage("images/Constructionhub_Logo_02.png"),
                    )),
              ),
              Opacity(
                opacity: 0.5,
                child: Container(
                  color: Colors
                      .white, // Optional: Add a color overlay for better visibility
                ),
              ),
              ListView(
                children: [
                  const Center(
                    child: Text(
                      "Size and Location",
                      style: TextStyle(
                          fontSize: 28.0, fontWeight: FontWeight.w600),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Size in Sq Feet",
                      style: TextStyle(
                          fontSize: 18.0, fontWeight: FontWeight.w600),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 8.0, right: 8.0),
                    child: TextField(
                      controller: size,
                      decoration:
                          InputDecoration(label: Text("Enter Sq feet Here")),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Location",
                      style: TextStyle(
                          fontSize: 18.0, fontWeight: FontWeight.w600),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 8.0, right: 8.0),
                    child: TextField(
                      controller: province,
                      decoration:
                          InputDecoration(label: Text("Enter Province Here")),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 8.0, right: 8.0),
                    child: TextField(
                      controller: city,
                      decoration:
                          InputDecoration(label: Text("Enter City Here")),
                    ),
                  ),
                  ElevatedButton(
                      onPressed: () => {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Budget(
                                        title: title,
                                        des: des,
                                        type: type,
                                        size: size.text,
                                        province: province.text,
                                        city: city.text)))
                          },
                      child: const Text("Next")),
                ],
              )
            ],
          ),
        ));
  }
}
