import 'package:flutter/material.dart';
import 'package:fyp_construction_hub/Auth%20Services/auth_services.dart';
import 'package:fyp_construction_hub/commons/common_button.dart';
import 'package:fyp_construction_hub/pages/Get%20Material/getMaterial.dart';
import 'package:fyp_construction_hub/pages/Project%20Owner/Upload%20a%20project/detail_into.dart';
import 'package:fyp_construction_hub/pages/Project%20Owner/Upload%20a%20project/upload_project.dart';
import 'package:fyp_construction_hub/pages/get_services.dart';
import 'package:fyp_construction_hub/pages/Project%20Owner/manageProjects.dart';

class ProjectOwner extends StatelessWidget {
  ProjectOwner({super.key});
  AuthServices services = AuthServices();
  @override
  Widget build(BuildContext context) {
    void getProjects(){
      services.GetProjects(context: context);
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Project Owner Dashboard',
          style: TextStyle(
            color: Colors.white,
          ),),
        backgroundColor: const Color.fromRGBO(93, 93, 93, 1.0),
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/background.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: ListView(children: [
          Padding(
            padding: const EdgeInsets.all(40.0),
            child: Container(
              height: 130.0,
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/Constructionhub_Logo_02.png"),
                    fit: BoxFit.fitHeight),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 100.0, right: 100.0, bottom: 20.0),
            child: CustomButton(
              title: "Upload a Project",
              onPress: () => {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => const DetailsInfoo()))
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 100.0, right: 100.0, bottom: 20.0),
            child: CustomButton(
              title: "Manage Projects",
              onPress: () => {
                  getProjects()
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 100.0, right: 100.0, bottom: 20.0),
            child: CustomButton(
              title: "Get Services",
              onPress: () => {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => const GetServices()))
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 100.0, right: 100.0, bottom: 20.0),
            child: CustomButton(
              title: "Get Material",
              onPress: () => {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => const GetMaterial()))
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 100.0, right: 100.0),
            child: CustomButton(
              title: "Feedback",
              onPress: () => {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => ProjectOwner()))
              },
            ),
          ),
        ]),
      ),
    );
  }
}
