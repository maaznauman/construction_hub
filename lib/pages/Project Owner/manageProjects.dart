import 'package:flutter/material.dart';
import 'package:fyp_construction_hub/Providers/owner_project_provider.dart';
import 'package:fyp_construction_hub/commons/listingView.dart';
import 'package:provider/provider.dart';

class ManageProjects extends StatefulWidget {
  const ManageProjects({Key? key}) : super(key: key);

  @override
  State<ManageProjects> createState() => _ManageProjectsState();
}

class _ManageProjectsState extends State<ManageProjects> {
  @override
  Widget build(BuildContext context) {
    final listings = Provider.of<ProjectProvider>(context).listings;
    return listings.isNotEmpty
        ? Scaffold(
      // Display Scaffold if listing is available
      appBar: AppBar(
        title: const Text(
          'Manage Projects',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromRGBO(93, 93, 93, 1.0),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Column(
          children: [
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                'Uploaded Projects',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: listings.length,
                itemBuilder: (context, index) {
                  // Display your listing items here
                  final item = listings[index];
                  return Column(
                    children: [
                      item.status=="uploaded" ? ListingView(
                        completeTitle: item.title,
                        completedescription: item.description,
                        type: item.type,
                        size: item.size,
                        province: item.province,
                        city: item.city,
                        UserId: item.UserId,
                        id: item.id,
                        budget: item.budget,
                        status: item.status,
                        title: item.title.length > 20
                            ? item.title.substring(0, 20)
                            : item.title,
                        description: item.description.length >= 55
                            ? "${item.description.substring(0, 55)}..."
                            : item.description,
                        time: item.time,
                        breakdown1: item.breakdown1,
                        breakdown2: item.breakdown2,
                        breakdown3: item.breakdown3,
                      ) : const SizedBox(),
                      const SizedBox(
                        height: 10.0,
                      )
                    ],
                  );
                },
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                'Running Projects',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: listings.length,
                itemBuilder: (context, index) {
                  // Display your listing items here
                  final item = listings[index];
                  return Column(
                    children: [
                      item.status=="running"? ListingView(
                        completeTitle: item.title,
                        completedescription: item.description,
                        type: item.type,
                        size: item.size,
                        province: item.province,
                        city: item.city,
                        UserId: item.UserId,
                        id: item.id,
                        budget: item.budget,
                        status: item.status,
                        title: item.title.length > 20
                            ? item.title.substring(0, 20)
                            : item.title,
                        description: item.description.length >= 55
                            ? "${item.description.substring(0, 55)}..."
                            : item.description,
                        time: item.time,
                        breakdown1: item.breakdown1,
                        breakdown2: item.breakdown2,
                        breakdown3: item.breakdown3,
                      ):Container(),
                      const SizedBox(
                        height: 10.0,
                      )
                    ],
                  );
                },
              ),
            ),

            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                'Bids',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: listings.length,
                itemBuilder: (context, index) {
                  // Display your listing items here
                  final item = listings[index];
                  return ListView.builder(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          itemCount: item.bid.length,
                          itemBuilder: (context, index){
                            final bid = item.bid[index];
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                height: 50.0,
                                width: 200.0,
                                decoration: BoxDecoration(
                                  border: Border.all(),
                                  borderRadius: BorderRadius.circular(10.0),
                                  image: DecorationImage(
                                    image: AssetImage("images/back.jpg"),
                                    fit: BoxFit.fill
                                  ),
                                ),
                                child: Column(children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top:28.0),
                                    child: Text(item.title, style: TextStyle(
                                      fontSize: 22.0,
                                      color: Colors.white
                                    ),),
                                  ),
                                  Text("Bid Amount: ${bid.price}", style: TextStyle(
                                    fontSize: 18.0,
                                      color: Colors.white
                                  ),),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 5.0),
                                    child: ElevatedButton(onPressed: (){}, child: Text("Accept offer")),
                                  )
                                ],),
                              ),
                            );
                          }
                      );
                },
              ),
            )
          ],
        ),
      ),
    )
        : Scaffold(
      appBar: AppBar(
        title: const Text(
          'Manage Projects',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromRGBO(93, 93, 93, 1.0),
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 350.0, left: 120.0),
        child: const Column(
          children: [
            Text("No projects posted by you yet."),
          ],
        ),
      ),
    );
  }
}
