import 'package:flutter/material.dart';
import 'package:fyp_construction_hub/Auth%20Services/auth_services.dart';
import 'package:fyp_construction_hub/commons/common_button.dart';
import 'package:fyp_construction_hub/pages/Contractor/contractor.dart';
import 'package:fyp_construction_hub/pages/Project%20Owner/project_owner.dart';

class Home extends StatelessWidget {
  AuthServices services = AuthServices();
  Home({super.key});
  @override
  Widget build(BuildContext context) {
    void getProjects(){
      services.GetAllProjects(context: context);
    }
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text('Home Page',
        style: TextStyle(
          color: Colors.white,
        ),),
        backgroundColor: const Color.fromRGBO(93, 93, 93, 1.0),
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/background.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: ListView(children: [
          Padding(
            padding: const EdgeInsets.all(40.0),
            child: Container(
              height: 130.0,
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("images/Constructionhub_Logo_02.png"),
                    fit: BoxFit.fitHeight),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 100.0, right: 100.0, bottom: 20.0),
            child: CustomButton(
              title: "Project Owner",
              onPress: () => {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => ProjectOwner()))
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 100.0, right: 100.0, bottom: 20.0),
            child: CustomButton(
              title: "Contractor",
              onPress: () => {
                getProjects()
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 100.0, right: 100.0, bottom: 20.0),
            child: CustomButton(
              title: "Material Supplier",
              onPress: () => {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Home()))
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 100.0, right: 100.0, bottom: 20.0),
            child: CustomButton(
              title: "Architect",
              onPress: () => {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Home()))
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 100.0, right: 100.0),
            child: CustomButton(
              title: "Other Services",
              onPress: () => {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Home()))
              },
            ),
          ),
        ]),
      ),
    );
  }
}
