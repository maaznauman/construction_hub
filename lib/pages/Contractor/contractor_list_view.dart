import 'package:flutter/material.dart';
import 'package:fyp_construction_hub/pages/Contractor/Available%20Projects/bid.dart';

class ContractorListView extends StatefulWidget {
  final String completeTitle,
      completedescription,
      type,
      size,
      province,
      city,
      UserId,
      id,
      budget,
      title,
      description,
      time,
      breakdown1,
      breakdown2,
      breakdown3;
  ContractorListView(
      {super.key,
      required this.completeTitle,
      required this.completedescription,
      required this.city,
      required this.type,
      required this.time,
      required this.title,
      required this.budget,
      required this.breakdown2,
      required this.breakdown1,
      required this.breakdown3,
      required this.description,
      required this.id,
      required this.size,
      required this.province,
      required this.UserId});
  @override
  State<ContractorListView> createState() => _ContractorListViewState();
}

class _ContractorListViewState extends State<ContractorListView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200.0,
      width: 400.0,
      decoration:
          BoxDecoration(
              border: Border.all(color: Colors.black, width: 1.0),
              borderRadius: BorderRadius.circular(10.0),
              image: const DecorationImage(
                image: AssetImage("images/back.jpg"),
                fit: BoxFit.fill
              )
          ),
        
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(widget.title,style: const TextStyle(
              fontSize: 22.0,
                color: Colors.white
            ),),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("PKR: ${widget.budget}",style: const TextStyle(
                fontSize: 18.0,
              color: Colors.white
            ),),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(widget.description,style: const TextStyle(
                color: Colors.white
            ),),
          ),
          ElevatedButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=>Bid(id: widget.id,)));
          }, child: Padding(
            padding: const EdgeInsets.only(left: 22.0,right: 22.0),
            child: const Text("BID"),
          ))
        ],
      ),
    );
  }
}
