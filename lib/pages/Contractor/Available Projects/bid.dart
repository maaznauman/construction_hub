import 'package:flutter/material.dart';
import 'package:fyp_construction_hub/Auth%20Services/auth_services.dart';
class Bid extends StatelessWidget {
  final String id;
  Bid({super.key, required this.id});
  AuthServices services = AuthServices();
  TextEditingController _bid = TextEditingController();
  @override
  Widget build(BuildContext context) {
    void postBid(){
      services.addBid(context: context, id: id, price: _bid.text);
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Place Bid',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromRGBO(93, 93, 93, 1.0),
      ),
      body: Container(
        margin: EdgeInsets.only(top: 300.0, left: 50.0, right: 50.0),
        child: ListView(children: [
          Text("Enter The Bid Amount"),
          TextField(
            keyboardType: TextInputType.number,
            controller: _bid,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 20.0),
            child: ElevatedButton(onPressed: (){
              postBid();
            }, child: Text("Place Bid")),
          )
        ],),
      ),
    );
  }
}
