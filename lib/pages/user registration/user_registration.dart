import 'package:flutter/material.dart';
import 'package:fyp_construction_hub/Auth%20Services/auth_services.dart';
import 'package:fyp_construction_hub/commons/text_field.dart';
import 'package:fyp_construction_hub/pages/Home/home.dart';
import 'package:fyp_construction_hub/pages/loginpage/login.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignUpState();
}

enum Gender { Male, Female, Other }

class _SignUpState extends State<SignUp> {
  TextEditingController emailController = TextEditingController();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController contactInfoController = TextEditingController();
  TextEditingController genderController = TextEditingController();
  AuthServices services = AuthServices();
  Gender? _gender = null;
  @override
  Widget build(BuildContext context) {
    void SignUp() {

      services.SignUpUser(
          context: context,
          firstName: firstNameController.text,
          lastName: lastNameController.text,
          email: emailController.text,
          password: passwordController.text,
          contactInfo: contactInfoController.text,
          gender: genderController.text);
    }

    return Scaffold(
      // backgroundColor: Colors.transparent,
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage("images/background.jpg"),
              fit: BoxFit.cover),
        ),
        child: ListView(children: [
          Container(
            height: 100.0,
            decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("images/Constructionhub_Logo_02.png"),
                  fit: BoxFit.fitHeight),
            ),
          ),
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CustomTextField(
                      controller: firstNameController, label: "First Name"),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CustomTextField(
                      controller: lastNameController, label: "Last Name"),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CustomTextField(
                      controller: emailController, label: "Emaill"),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CustomTextField(
                      controller: passwordController, label: "Password"),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CustomTextField(
                controller: confirmPasswordController, label: "Confirm Password"),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CustomTextField(
                controller: contactInfoController, label: "Contact Info"),
          ),
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: (BorderRadius.circular(20.0)),
                        border: Border.all(color: Colors.white)),
                    child: ListTile(
                      title: const Text(
                        "Male",
                        style: TextStyle(fontSize: 20.0, color: Colors.white),
                      ),
                      leading: Radio(
                          value: Gender.Male,
                          groupValue: _gender,
                          onChanged: (Gender? value) {
                            setState(() {
                              _gender = value;
                            });
                            genderController.text = "Male";
                          }),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: (BorderRadius.circular(20.0)),
                        border: Border.all(color: Colors.white)),
                    child: ListTile(
                      title: const Text(
                        "Female",
                        style: TextStyle(color: Colors.white, fontSize: 20.0),
                      ),
                      leading: Radio(
                          value: Gender.Female,
                          groupValue: _gender,
                          onChanged: (Gender? value) {
                            setState(() {
                              _gender = value;
                            });
                            genderController.text = "Female";
                          }),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 100.0, right: 100.0),
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(backgroundColor: Colors.black54),
                onPressed: () {
                  SignUp();
                },
                child: const Text(
                  "Sign Up",
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                  ),
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 100.0, right: 100.0),
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(backgroundColor: Colors.black54),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>LogIn()));
                },
                child: const Text(
                  "Login",
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                  ),
                )),
          ),
        ],),
      ),
    );
  }
}
