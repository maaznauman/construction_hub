import 'package:flutter/material.dart';
import 'package:fyp_construction_hub/commons/listingView.dart';
class GetMaterial extends StatefulWidget {
  const GetMaterial({Key? key}) : super(key: key);

  @override
  State<GetMaterial> createState() => _GetMaterialState();
}

class _GetMaterialState extends State<GetMaterial> {
  @override
  Widget build(BuildContext context) {
    final listings = [];
    return listings.isNotEmpty
        ? Scaffold(
      // Display Scaffold if listing is available
      appBar: AppBar(
        title: const Text('Get Materials',
          style: TextStyle(
            color: Colors.white,
          ),),
        backgroundColor: const Color.fromRGBO(93, 93, 93, 1.0),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Material(
          child: ListView.builder(
            itemCount: listings.length,
            itemBuilder: (context, index) {
              // Display your listing items here
              final item = listings[index];
              return Column(
                children: [
              ListingView(
                  completeTitle: item.title,
                  completedescription: item.description,
                  type: item.type,
                  status: item.status,
                  size: item.size,
                  province: item.province,
                  city: item.city,
                  UserId: item.UserId,
                  id: item.id,
                  budget: item.budget,
                  title: item.title.length > 20
                      ? item.title.substring(0, 20)
                      : item.title,
                  description: item.description.length >= 55
                      ? "${item.description.substring(0, 55)}..."
                      : item.description,
                  time: item.time,
                  breakdown1: item.breakdown1,
                  breakdown2: item.breakdown2,
                  breakdown3: item.breakdown3),
                  const SizedBox(
                    height: 10.0,
                  )
                ],
              );
            },
          ),
        ),
      ),
    )
        : Scaffold(
      appBar: AppBar(
        title: const Text('Get Materials',
          style: TextStyle(
            color: Colors.white,
          ),),
        backgroundColor: const Color.fromRGBO(93, 93, 93, 1.0),
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 350.0, left: 120.0),
        child: Column(
          children: [
            const Text("No projects posted by you yet."),
          ],
        ),
      ),
    );
  }
}