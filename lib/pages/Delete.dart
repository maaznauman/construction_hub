import 'package:flutter/material.dart';
import 'package:fyp_construction_hub/Auth%20Services/auth_services.dart';

class DeleteListings extends StatelessWidget {
  final id;
  DeleteListings({super.key, this.id});
  AuthServices services = AuthServices();
  @override
  Widget build(BuildContext context) {
    void delete() {
      print(id);
      services.DeleteProject(context: context, id: id);
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Delete Project',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromRGBO(93, 93, 93, 1.0),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 300.0),
        child: ListView(
          children: [
            const Center(
                child:
                    Text("Are you sure you want to Delete the listing?")),
            Padding(
              padding: const EdgeInsets.only(left: 100.0),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                        onPressed: () {
                          delete();
                        },
                        child: const Text("Delete")),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                        onPressed: () {}, child: const Text("Cancel")),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
