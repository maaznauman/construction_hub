import 'dart:convert';

class BidModel {
  String UserId;
  String price;
  BidModel(
      this.UserId,
      this.price,);

  Map<String, dynamic> toMap() {
    return {
      'userId': UserId,
      'price': price,
    };
  }

  factory BidModel.fromMap(Map<String, dynamic> map) {
    return BidModel(
      map['UserId'] ?? '',
      map['price'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory BidModel.fromJson(String source) =>
      BidModel.fromMap(json.decode(source));
}
