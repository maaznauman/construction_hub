import 'dart:convert';
class UserModel{
  String id;
  String firstName;
  String lastName;
  String email;
  String password;
  String contactInfo;
  String gender;


  UserModel(this.id,this.firstName, this.lastName, this.email, this.password, this.contactInfo, this.gender);

  Map<String, dynamic> toMap(){
    return {
      'id':'$id',
      'firstName':'$firstName',
      'lastName':'$lastName',
      'email':'$email',
      'password':'$password',
      'contactInfo':'$contactInfo',
      'gender':'$gender',
    };
  }
  factory UserModel.fromMap(Map<String, dynamic> map){
    return UserModel(
      map['_id'] ?? '',
      map['firstName'] ?? '',
      map['lastName'] ?? '',
      map['email'] ?? '',
      map['password'] ?? '',
      map['contactInfo'] ?? '',
      map['gender'] ?? '',
    );
  }
  String toJson() => json.encode(toMap());
  factory UserModel.fromJson(String source) => UserModel.fromMap(json.decode(source));
}