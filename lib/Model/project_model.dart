import 'dart:convert';

import 'package:fyp_construction_hub/Model/bid_model.dart';

class ProjectModel {
  String id;
  String UserId;
  String title;
  String description;
  String type;
  String size;
  String province;
  String city;
  String budget;
  String time;
  String breakdown1;
  String breakdown2;
  String breakdown3;
  String status;
  List<BidModel> bid;

  ProjectModel(
      this.id,
      this.UserId,
      this.title,
      this.description,
      this.type,
      this.size,
      this.province,
      this.city,
      this.budget,
      this.time,
      this.breakdown1,
      this.breakdown2,
      this.breakdown3,
      this.status,
      this.bid);

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'userId': UserId,
      'title': title,
      'description': description,
      'type': type,
      'size': size,
      'province': province,
      'city': city,
      'budget': budget,
      'time': time,
      'breakdown1': breakdown1,
      'breakdown2': breakdown2,
      'breakdown3': breakdown3,
      'status': status,
      'bid': bid,
    };
  }

  factory ProjectModel.fromMap(Map<String, dynamic> map) {
    return ProjectModel(
      map['id'] ?? '',
      map['UserId'] ?? '',
      map['title'] ?? '',
      map['description'] ?? '',
      map['type'] ?? '',
      map['size'] ?? '',
      map['province'] ?? '',
      map['city'] ?? '',
      map['budget'] ?? '',
      map['time'] ?? '',
      map['breakdown1'] ?? '',
      map['breakdown2'] ?? '',
      map['breakdown3'] ?? '',
      map['status'] ?? '',
      List<BidModel>.from(map['bid'] ?? []),
    );
  }

  String toJson() => json.encode(toMap());

  factory ProjectModel.fromJson(String source) =>
      ProjectModel.fromMap(json.decode(source));
}
