import 'package:flutter/material.dart';
import 'package:fyp_construction_hub/Providers/owner_project_provider.dart';
import 'package:fyp_construction_hub/Providers/user_provider.dart';
import 'package:fyp_construction_hub/pages/loginpage/login.dart';
import 'package:provider/provider.dart';
void main() {
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(create: (context) => UserProvider()),
    ChangeNotifierProvider(create: (context) => ProjectProvider()),

  ], child: MyApp()));
}
class MyApp extends StatelessWidget { 
  const MyApp({super.key});
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Construction Hub',
      home: Scaffold(
          body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
          image: AssetImage("images/background.jpg"),
          fit: BoxFit.cover,
        )),
        child: const LogIn(),
      )),
    );
  }
}
