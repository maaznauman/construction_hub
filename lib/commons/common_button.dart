import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  VoidCallback onPress;
  final String title;
  CustomButton({super.key, required this.onPress, required this.title});
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(backgroundColor: Colors.black),
        onPressed: () {
          onPress();
        },
        child: Text(
          title,
          style: const TextStyle(
            fontSize: 18,
            color: Colors.white,
          ),
        ));
  }
}
