import 'package:flutter/material.dart';
class CustomText extends StatelessWidget
{
  // ignore: prefer_typing_uninitialized_variables
  final text;
  // ignore: prefer_typing_uninitialized_variables
  final fontWeight;
  // ignore: prefer_typing_uninitialized_variables
  final fontSize;
  // ignore: prefer_typing_uninitialized_variables
  final color;
  const CustomText({Key? key,required this.fontWeight,required this.fontSize,required this.color,required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      '$text',
      style: TextStyle(
          fontWeight: fontWeight,
          fontSize: fontSize,
          color: color),
    );
  }
}