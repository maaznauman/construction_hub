import 'package:flutter/material.dart';
import 'package:fyp_construction_hub/pages/Delete.dart';
// import 'package:pet_app/Screens/MarketPlace%20Menu/detailed_view_listings.dart';
import './customText.dart';
import './sizeManager.dart';

class ListingView extends StatelessWidget {
  String id;
  String UserId;
  String title;
  String description;
  String type;
  String size;
  String province;
  String city;
  String budget;
  String time;
  String breakdown1;
  String breakdown2;
  String breakdown3;
  String completeTitle;
  String completedescription;
  String status;
  // ignore: prefer_typing_uninitialized_variables
  ListingView(
      {Key? key,
        required this.id,
        required this.description,
        required this.title,
        required this.type,
        required this.size,
        required this.province,
        required this.city,
        required this.budget,
        required this.time,
        required this.breakdown1,
        required this.breakdown2,
        required this.breakdown3,
        required this.completeTitle,
        required this.completedescription,
      required this.UserId,
      required this.status})
      : super(key: key);
  @override
  Widget build(BuildContext context) {

    var sizemanager = SizeManager(context);
    return Container(
      margin: EdgeInsets.only(
          left: sizemanager.scaledWidth(0.5), right: sizemanager.scaledWidth(2)),
      height: 200.0,
      width: 300.0,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black, width: 1.0),
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
          image: AssetImage("images/back.jpg"),
          fit: BoxFit.cover
        )
      ),
      child: ElevatedButton(

        onPressed: () {

        },
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.transparent,
          foregroundColor: Colors.black,
          elevation: 0,
          side: const BorderSide(
            width: 1.0,
            color: Colors.black12,
          ),
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                children: [
                  Center(
                    child: CustomText(
                        fontWeight: FontWeight.w400,
                        fontSize:20.0,
                        color: Colors.white,
                        text: title),
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          bottom:20.0),
                      child: CustomText(
                          fontWeight: FontWeight.w400,
                          fontSize: 18.0,
                          color: Colors.white,
                          text: "Budget: $budget"),
                    ),
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(
                           bottom:0.0),
                      child: CustomText(
                          fontWeight: FontWeight.w300,
                          fontSize: 14.0,
                          color: Colors.white,
                          text: description),
                    ),
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          bottom:0.0),
                      child: CustomText(
                          fontWeight: FontWeight.w300,
                          fontSize: 14.0,
                          color: Colors.white,
                          text: "status: $status"),
                    ),
                  ),
                  Center(
                      child: ElevatedButton(onPressed: (){

                      Navigator.push(context, MaterialPageRoute(builder: (context)=>DeleteListings(id:id)));
                    }, child: const Text("Delete")),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}